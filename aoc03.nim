
import aoc_util
import strutils
import sets
import sequtils

const 
    SOURCE_FILE = getSourceFile("day3-rucksack")
    LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

proc main*() =

    var sum = 0
    for line in SOURCE_FILE.lines:
        let line_len = line.len
        let half_len = line_len div 2
        let letters_1 = toHashSet(line[0..half_len-1])
        let letters_2 = toHashSet(line[half_len .. line_len-1])

        var unique_letters = intersection(letters_1, letters_2)
        let unique_letter = unique_letters.pop()

        sum += LETTERS.find(unique_letter)+1

    echo sum

    let lines = SOURCE_FILE.lines.toSeq

    var sum_2 = 0

    for item_sets in lines.distribute(lines.len div 3, false):
        let sets = item_sets.mapIt(toHashSet(it))
        var uniques = sets.foldr(intersection(a, b))
        let unique = uniques.pop()
        
        sum_2 += LETTERS.find(unique)+1

    echo sum_2

when isMainModule:
    main()
