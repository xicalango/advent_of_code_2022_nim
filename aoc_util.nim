
const
    stem = "/home/alexx/Development/advent-of-code-2022/res/"

proc getSourceFile*(file_stem: string, extension: string = ".txt"): string =
    result = stem
    result.add(file_stem)

    when `not` defined(USE_DATA):
        result.add("_example")

    result.add(extension)

