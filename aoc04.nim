
import aoc_util
import strutils
import sequtils

const
    SOURCE_FILE = getSourceFile("day4-ranges")

type
    Ranges = object
        start1: int
        end1: int
        start2: int
        end2: int

proc toRange(value: openArray[int]): Ranges =
    result = Ranges(start1: value[0], end1: value[1], start2: value[2], end2: value[3])

proc findOverlap(ranges: Ranges): array[0..1, int] =
    let max_start = max(ranges.start1, ranges.start2)
    let min_end = min(ranges.end1, ranges.end2)
    result = [max_start, min_end]

proc partiallyContains(ranges: Ranges): bool =
    result = ranges.start1 <= ranges.end2 and ranges.start2 <= ranges.end1

proc fullyContains(ranges: Ranges): bool =
    let overlap = ranges.findOverlap()
    result = (ranges.start1 == overlap[0] and ranges.end1 == overlap[1]) or (ranges.start2 == overlap[0] and ranges.end2 == overlap[1])

proc main*() =

    var fully_counter = 0
    var partial_counter = 0

    for line in SOURCE_FILE.lines:
        let splitted = line.split({'-',','}, 4).mapIt(it.parseInt).toRange()
        if splitted.fullyContains():
            fully_counter += 1
        if splitted.partiallyContains():
            partial_counter += 1
    
    echo "fully: ", fully_counter
    echo "partial: ", partial_counter

when isMainModule:
    main()
