
import std/monotimes
import std/times
import std/macros

import aoc01
import aoc02
import aoc03
import aoc04

macro bench_run(arg: untyped) = 
    arg.expectKind nnkDotExpr
    let name = newLit(arg[0].repr)

    result = quote do:
        echo "start: ", `name`
        let start_time = getMonoTime()
        `arg`()
        let end_time = getMonoTime()
        echo "end: ", `name`, ": ", end_time - start_time

proc main*() =
    bench_run(aoc01.main)
    bench_run(aoc02.main)
    bench_run(aoc03.main)
    bench_run(aoc04.main)

when isMainModule:
    main()
