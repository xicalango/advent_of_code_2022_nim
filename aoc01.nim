
import aoc_util

import std/strutils
import std/sequtils
import std/algorithm

const SOURCE_FILE = getSourceFile("day1-calories")

template sum(input: untyped): untyped = foldr(input, a + b)

proc main*() =
    var calories = newSeq[int64]()
    var cur_calories = newSeq[int64]()


    for line in SOURCE_FILE.lines:
        var line = line
        line.stripLineEnd()

        if line.len == 0:
            calories.add(cur_calories.sum)
            cur_calories = newSeq[int64]()
            continue

        cur_calories.add(parseInt(line))

    if cur_calories.len > 0:
        calories.add(cur_calories.sum)

    sort(calories)

    echo calories.max
    echo calories[^3 .. ^1].sum

when isMainModule:
    main()
