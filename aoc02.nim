
import std/strutils
import aoc_util

type
    Hand = enum
        rock, paper, scissors
    Round = tuple[self: Hand, other: Hand]
    Strategy = enum
        lose, draw, win

proc `<`(h1: Hand, h2: Hand): bool = return case h1:
        of Hand.rock: h2 == Hand.paper
        of Hand.paper: h2 == Hand.scissors
        of Hand.scissors: h2 == Hand.rock

proc winsAgainst(hand: Hand): Hand = return case hand:
        of Hand.rock: Hand.paper
        of Hand.paper: Hand.scissors
        of Hand.scissors: Hand.rock

proc losesAgainst(hand: Hand): Hand = return case hand:
        of Hand.rock: Hand.scissors
        of Hand.paper: Hand.rock
        of Hand.scissors: Hand.paper

proc valueOf(hand: Hand): int = return case hand:
    of Hand.rock: 1
    of Hand.paper: 2
    of Hand.scissors: 3

proc parseHand(value: char): Hand = return case value:
    of 'A', 'X': Hand.rock
    of 'B', 'Y': Hand.paper
    of 'C', 'Z': Hand.scissors
    else: raise newException(ValueError, "invalid value")

proc parseStrategy(value: char): Strategy = return case value:
        of 'X': Strategy.lose
        of 'Y': Strategy.draw
        of 'Z': Strategy.win
        else: raise newException(ValueError, "invalid value")

proc apply(strategy: Strategy, opponent_hand: Hand): Hand = return case strategy:
        of Strategy.draw:
            opponent_hand
        of Strategy.lose:
            losesAgainst(opponent_hand)
        of Strategy.win:
            winsAgainst(opponent_hand) 

proc parseRound(value: string): Round =
    let values = value.split(' ', 2)
    (values[1][0].parseHand, values[0][0].parseHand)

proc parseRoundWithStrategy(value: string): Round =
    let values = value.split(' ', 2)
    let opponend_hand = values[0][0].parseHand
    let my_strategy = values[1][0].parseStrategy
    let my_hand = my_strategy.apply(opponend_hand)
    (my_hand, opponend_hand)

proc valueOf(round: Round): int = return case cmp(round.self, round.other):
    of -1: 0
    of 0: 3
    of 1: 6
    else: raiseAssert("unreachable")

proc score(round: Round): int =
    let round_value = valueOf(round)

    let my_score = valueOf(round.self)

    round_value + my_score

proc main*() = 
    var accu_1 = 0
    var accu_2 = 0

    for line in getSourceFile("day2-guide").lines:
        var line = line
        line.stripLineEnd()
        let round = line.parseRound()
        accu_1 += round.score

        let round_with_strategy= line.parseRoundWithStrategy()
        accu_2 += round_with_strategy.score

    echo accu_1
    echo accu_2

when isMainModule:
    main()
